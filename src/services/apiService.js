import axios from 'axios'
import authHeader from "./authHeader";

let API_URL = "https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3";
//let API_URL = "https://api.yelp.com/v3";

class apiService {

    async fetchData(endpoint, params) {

        const instance = axios.create({
            baseURL: API_URL,
            headers: authHeader()
        });

        // or: instance.defaults.headers.common['header'] = TOKEN;

        return await instance.get(endpoint+params)
            .then((result) => {
                console.log("result.data");
                return result.data
            }).catch(function (error) {
                // handle error
                console.log(error);
            })
    }
}

export default new apiService();