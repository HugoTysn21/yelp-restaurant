import firebase from 'firebase/app'
import 'firebase/database'

// Get a Firestore instance
export const db = firebase
  .initializeApp({ databaseURL: "https://yelp-restaurant-9fe51-default-rtdb.firebaseio.com/",
  projectId: "yelp-restaurant-9fe51", }).database()