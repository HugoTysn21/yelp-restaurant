import Vue from 'vue'
import Vuex from 'vuex'
import apiService from "../services/apiService";
import {db} from '../db.js'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        details: {
            "id": "f-m7-hyFzkf0HSEeQ2s-9A",
            "alias": "fog-harbor-fish-house-san-francisco-2",
            "name": "Fog Harbor Fish House",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/2eAtP1SJy21JTvQWxaQSng/o.jpg",
            "is_claimed": true,
            "is_closed": false,
            "url": "https://www.yelp.com/biz/fog-harbor-fish-house-san-francisco-2?adjust_creative=yKYbm1ATzJHgteZkEgBKvw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=yKYbm1ATzJHgteZkEgBKvw",
            "phone": "+14154212442",
            "display_phone": "(415) 421-2442",
            "review_count": 6366,
            "categories": [
                {
                    "alias": "seafood",
                    "title": "Seafood"
                },
                {
                    "alias": "wine_bars",
                    "title": "Wine Bars"
                },
                {
                    "alias": "cocktailbars",
                    "title": "Cocktail Bars"
                }
            ],
            "rating": 4.5,
            "location": {
                "address1": "39 Pier",
                "address2": "Ste A-202",
                "address3": "",
                "city": "San Francisco",
                "zip_code": "94133",
                "country": "US",
                "state": "CA",
                "display_address": [
                    "39 Pier",
                    "Ste A-202",
                    "San Francisco, CA 94133"
                ],
                "cross_streets": ""
            },
            "coordinates": {
                "latitude": 37.80885,
                "longitude": -122.41023
            },
            "photos": [
                "https://s3-media2.fl.yelpcdn.com/bphoto/2eAtP1SJy21JTvQWxaQSng/o.jpg",
                "https://s3-media4.fl.yelpcdn.com/bphoto/rlCBCXPmrJdjawepDRSYJA/o.jpg",
                "https://s3-media2.fl.yelpcdn.com/bphoto/h4gfkR9juEcypQe3WS0LMw/o.jpg"
            ],
            "price": "$$",
            "hours": [
                {
                    "open": [
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 0
                        },
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 1
                        },
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 2
                        },
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 3
                        },
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 4
                        },
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 5
                        },
                        {
                            "is_overnight": false,
                            "start": "1100",
                            "end": "2100",
                            "day": 6
                        }
                    ],
                    "hours_type": "REGULAR",
                    "is_open_now": false
                }
            ],
            "transactions": [
                "restaurant_reservation"
            ],
            "messaging": {
                "url": "https://www.yelp.com/raq/f-m7-hyFzkf0HSEeQ2s-9A?adjust_creative=yKYbm1ATzJHgteZkEgBKvw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=yKYbm1ATzJHgteZkEgBKvw#popup%3Araq",
                "use_case_text": "Message the Business"
            }
        },
        restaurant: {
            "businesses": [
                {
                    "id": "Xg-FyjVKAN70LO4u4Z1ozg",
                    "alias": "hog-island-oyster-co-san-francisco",
                    "name": "Hog Island Oyster Co",
                    "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/TW9FgV_Ufqd15t_ARQuz1A/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/hog-island-oyster-co-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 6117,
                    "categories": [
                        {
                            "alias": "seafood",
                            "title": "Seafood"
                        },
                        {
                            "alias": "seafoodmarkets",
                            "title": "Seafood Markets"
                        },
                        {
                            "alias": "raw_food",
                            "title": "Live/Raw Food"
                        }
                    ],
                    "rating": 4.5,
                    "coordinates": {
                        "latitude": 37.795831,
                        "longitude": -122.393303
                    },
                    "transactions": [],
                    "price": "$$",
                    "location": {
                        "address1": "1 Ferry Bldg",
                        "address2": "",
                        "address3": "Shop 11",
                        "city": "San Francisco",
                        "zip_code": "94111",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "1 Ferry Bldg",
                            "Shop 11",
                            "San Francisco, CA 94111"
                        ]
                    },
                    "phone": "+14153917117",
                    "display_phone": "(415) 391-7117",
                    "distance": 1154.8167382059307
                },
                {
                    "id": "PsY5DMHxa5iNX_nX0T-qPA",
                    "alias": "kokkari-estiatorio-san-francisco",
                    "name": "Kokkari Estiatorio",
                    "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/7StMTfE2srlcIu2opeTb3g/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/kokkari-estiatorio-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 4559,
                    "categories": [
                        {
                            "alias": "greek",
                            "title": "Greek"
                        },
                        {
                            "alias": "mediterranean",
                            "title": "Mediterranean"
                        }
                    ],
                    "rating": 4.5,
                    "coordinates": {
                        "latitude": 37.796996,
                        "longitude": -122.399661
                    },
                    "transactions": [
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$$",
                    "location": {
                        "address1": "200 Jackson St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94111",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "200 Jackson St",
                            "San Francisco, CA 94111"
                        ]
                    },
                    "phone": "+14159810983",
                    "display_phone": "(415) 981-0983",
                    "distance": 1124.9562174585888
                },
                {
                    "id": "lJAGnYzku5zSaLnQ_T6_GQ",
                    "alias": "brendas-french-soul-food-san-francisco-5",
                    "name": "Brenda's French Soul Food",
                    "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/sNIJnePGDenUOyewsD8tLg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/brendas-french-soul-food-san-francisco-5?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 11061,
                    "categories": [
                        {
                            "alias": "breakfast_brunch",
                            "title": "Breakfast & Brunch"
                        },
                        {
                            "alias": "southern",
                            "title": "Southern"
                        },
                        {
                            "alias": "cajun",
                            "title": "Cajun/Creole"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.7829016035273,
                        "longitude": -122.419043442957
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "652 Polk St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94102",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "652 Polk St",
                            "San Francisco, CA 94102"
                        ]
                    },
                    "phone": "+14153458100",
                    "display_phone": "(415) 345-8100",
                    "distance": 1733.4379946566146
                },
                {
                    "id": "_EncdQezAzcShATMFXL0dA",
                    "alias": "tropisueno-san-francisco-8",
                    "name": "Tropisueno",
                    "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/hUZgDmue1VRfvIXai6I0wg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/tropisueno-san-francisco-8?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 4443,
                    "categories": [
                        {
                            "alias": "cocktailbars",
                            "title": "Cocktail Bars"
                        },
                        {
                            "alias": "tacos",
                            "title": "Tacos"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.7853008468227,
                        "longitude": -122.403918653727
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "75 Yerba Buena Ln",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94103",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "75 Yerba Buena Ln",
                            "San Francisco, CA 94103"
                        ]
                    },
                    "phone": "+14152430299",
                    "display_phone": "(415) 243-0299",
                    "distance": 388.84095376614835
                },
                {
                    "id": "WOVHQYzSAdyXYOiYDTmujQ",
                    "alias": "the-bird-san-francisco",
                    "name": "The Bird",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/hzCuGZbkMWWJyX79UKfl7w/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/the-bird-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1983,
                    "categories": [
                        {
                            "alias": "chickenshop",
                            "title": "Chicken Shop"
                        },
                        {
                            "alias": "sandwiches",
                            "title": "Sandwiches"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.78724,
                        "longitude": -122.39995
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$",
                    "location": {
                        "address1": "115 New Montgomery St",
                        "address2": null,
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94105",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "115 New Montgomery St",
                            "San Francisco, CA 94105"
                        ]
                    },
                    "phone": "+14158729825",
                    "display_phone": "(415) 872-9825",
                    "distance": 41.667537325551386
                },
                {
                    "id": "bUr4iq2mKKiBOu2HKynylg",
                    "alias": "hrd-san-francisco-4",
                    "name": "HRD",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/G05B7mvYPHBpGu1wP1NK_Q/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/hrd-san-francisco-4?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 2505,
                    "categories": [
                        {
                            "alias": "korean",
                            "title": "Korean"
                        },
                        {
                            "alias": "asianfusion",
                            "title": "Asian Fusion"
                        },
                        {
                            "alias": "mexican",
                            "title": "Mexican"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.7811065758548,
                        "longitude": -122.395329724426
                    },
                    "transactions": [
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "521A 3rd St",
                        "address2": null,
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94107",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "521A 3rd St",
                            "San Francisco, CA 94107"
                        ]
                    },
                    "phone": "+14155432355",
                    "display_phone": "(415) 543-2355",
                    "distance": 760.8230840295653
                },
                {
                    "id": "zjlwTVZOyCU8OzzUPEavQQ",
                    "alias": "super-duper-burgers-san-francisco-3",
                    "name": "Super Duper Burgers",
                    "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/cIH_FO2pXX0fXir4LGebxQ/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/super-duper-burgers-san-francisco-3?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 2760,
                    "categories": [
                        {
                            "alias": "burgers",
                            "title": "Burgers"
                        },
                        {
                            "alias": "tradamerican",
                            "title": "American (Traditional)"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.786953,
                        "longitude": -122.403992
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "721 Market St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94103",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "721 Market St",
                            "San Francisco, CA 94103"
                        ]
                    },
                    "phone": "+14154078014",
                    "display_phone": "(415) 407-8014",
                    "distance": 353.3525015084877
                },
                {
                    "id": "uEeWn6sPq-giWvfE9uhQ7A",
                    "alias": "dotties-true-blue-cafe-san-francisco",
                    "name": "Dottie's True Blue Cafe",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/DP_jjHEs6WbdowWFum7UaQ/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/dotties-true-blue-cafe-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 4538,
                    "categories": [
                        {
                            "alias": "tradamerican",
                            "title": "American (Traditional)"
                        },
                        {
                            "alias": "breakfast_brunch",
                            "title": "Breakfast & Brunch"
                        },
                        {
                            "alias": "cafes",
                            "title": "Cafes"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.7817050429594,
                        "longitude": -122.409839630127
                    },
                    "transactions": [
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "28 6th St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94103",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "28 6th St",
                            "San Francisco, CA 94103"
                        ]
                    },
                    "phone": "+14158852767",
                    "display_phone": "(415) 885-2767",
                    "distance": 1040.8408583328028
                },
                {
                    "id": "u39mZEYojBiNic3lqKhPNw",
                    "alias": "tacorea-san-francisco",
                    "name": "Tacorea",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/wWmEPL-j9NwP6zPhMF-SHg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/tacorea-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1739,
                    "categories": [
                        {
                            "alias": "mexican",
                            "title": "Mexican"
                        },
                        {
                            "alias": "korean",
                            "title": "Korean"
                        },
                        {
                            "alias": "breakfast_brunch",
                            "title": "Breakfast & Brunch"
                        }
                    ],
                    "rating": 4.5,
                    "coordinates": {
                        "latitude": 37.7897794,
                        "longitude": -122.410717
                    },
                    "transactions": [],
                    "price": "$$",
                    "location": {
                        "address1": "809 Bush St",
                        "address2": "",
                        "address3": null,
                        "city": "San Francisco",
                        "zip_code": "94108",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "809 Bush St",
                            "San Francisco, CA 94108"
                        ]
                    },
                    "phone": "+14154848911",
                    "display_phone": "(415) 484-8911",
                    "distance": 997.3290565388144
                },
                {
                    "id": "bqSZc4Xh2o253DZXr5QOmg",
                    "alias": "marrakech-magic-theater-san-francisco-5",
                    "name": "Marrakech Magic Theater",
                    "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/axYhPu0DupRr0MIK8UNXdQ/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/marrakech-magic-theater-san-francisco-5?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1000,
                    "categories": [
                        {
                            "alias": "comedyclubs",
                            "title": "Comedy Clubs"
                        },
                        {
                            "alias": "magicians",
                            "title": "Magicians"
                        },
                        {
                            "alias": "dinnertheater",
                            "title": "Dinner Theater"
                        }
                    ],
                    "rating": 5.0,
                    "coordinates": {
                        "latitude": 37.785843,
                        "longitude": -122.411816
                    },
                    "transactions": [],
                    "price": "$$",
                    "location": {
                        "address1": "419 O'Farrell St",
                        "address2": null,
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94102",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "419 O'Farrell St",
                            "San Francisco, CA 94102"
                        ]
                    },
                    "phone": "+14157946893",
                    "display_phone": "(415) 794-6893",
                    "distance": 1050.3597762833895
                },
                {
                    "id": "8skVQNkVBv90iiPtFeSpzw",
                    "alias": "zero-zero-san-francisco",
                    "name": "Zero Zero",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/FIXb5NJNZVks5Q5EvvifXg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/zero-zero-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 3301,
                    "categories": [
                        {
                            "alias": "pizza",
                            "title": "Pizza"
                        },
                        {
                            "alias": "italian",
                            "title": "Italian"
                        },
                        {
                            "alias": "cocktailbars",
                            "title": "Cocktail Bars"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.78163,
                        "longitude": -122.40204
                    },
                    "transactions": [
                        "pickup"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "826 Folsom St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94107",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "826 Folsom St",
                            "San Francisco, CA 94107"
                        ]
                    },
                    "phone": "+14153488800",
                    "display_phone": "(415) 348-8800",
                    "distance": 611.7565988844583
                },
                {
                    "id": "ykvr3ib1gHQHI4tznSn8iA",
                    "alias": "wayfare-tavern-san-francisco-2",
                    "name": "Wayfare Tavern",
                    "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/PF0Gwry7SVfbI5tforgXgg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/wayfare-tavern-san-francisco-2?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 3956,
                    "categories": [
                        {
                            "alias": "tradamerican",
                            "title": "American (Traditional)"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.794169,
                        "longitude": -122.402359
                    },
                    "transactions": [
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$$",
                    "location": {
                        "address1": "558 Sacramento St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94111",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "558 Sacramento St",
                            "San Francisco, CA 94111"
                        ]
                    },
                    "phone": "+14157729060",
                    "display_phone": "(415) 772-9060",
                    "distance": 836.9848954281025
                },
                {
                    "id": "q2EbLD93gEO5uXXx7Pk3bw",
                    "alias": "benu-san-francisco-4",
                    "name": "Benu",
                    "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/NuJWvKMyw-PA6AtQvEOJGw/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/benu-san-francisco-4?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1078,
                    "categories": [
                        {
                            "alias": "newamerican",
                            "title": "American (New)"
                        },
                        {
                            "alias": "wine_bars",
                            "title": "Wine Bars"
                        },
                        {
                            "alias": "seafood",
                            "title": "Seafood"
                        }
                    ],
                    "rating": 4.5,
                    "coordinates": {
                        "latitude": 37.785402,
                        "longitude": -122.399068
                    },
                    "transactions": [
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$$$",
                    "location": {
                        "address1": "22 Hawthorne St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94105",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "22 Hawthorne St",
                            "San Francisco, CA 94105"
                        ]
                    },
                    "phone": "+14156854860",
                    "display_phone": "(415) 685-4860",
                    "distance": 182.7393176979489
                },
                {
                    "id": "-sg6DqQNGyTMt0MHoY7diQ",
                    "alias": "good-mong-kok-bakery-san-francisco",
                    "name": "Good Mong Kok Bakery",
                    "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/FPkV5W2n5tD1-NYgOfaGYg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/good-mong-kok-bakery-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 2788,
                    "categories": [
                        {
                            "alias": "bakeries",
                            "title": "Bakeries"
                        },
                        {
                            "alias": "dimsum",
                            "title": "Dim Sum"
                        },
                        {
                            "alias": "cantonese",
                            "title": "Cantonese"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.7954584839809,
                        "longitude": -122.408358365012
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$",
                    "location": {
                        "address1": "1039 Stockton St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94108",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "1039 Stockton St",
                            "San Francisco, CA 94108"
                        ]
                    },
                    "phone": "+14153972688",
                    "display_phone": "(415) 397-2688",
                    "distance": 1205.207295175229
                },
                {
                    "id": "tnhfDv5Il8EaGSXZGiuQGg",
                    "alias": "garaje-san-francisco",
                    "name": "Garaje",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/B5UcvKFCAMNZ5O8e-bx-pg/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/garaje-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1598,
                    "categories": [
                        {
                            "alias": "mexican",
                            "title": "Mexican"
                        },
                        {
                            "alias": "burgers",
                            "title": "Burgers"
                        },
                        {
                            "alias": "beerbar",
                            "title": "Beer Bar"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.781752952057,
                        "longitude": -122.396121970462
                    },
                    "transactions": [
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "475 3rd St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94107",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "475 3rd St",
                            "San Francisco, CA 94107"
                        ]
                    },
                    "phone": "+14156440838",
                    "display_phone": "(415) 644-0838",
                    "distance": 663.1309516727886
                },
                {
                    "id": "oT08T3Vpn1I7jDmrBBRMTw",
                    "alias": "house-of-prime-rib-san-francisco",
                    "name": "House of Prime Rib",
                    "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/HLrjaMoAgYSac0vx71YpCA/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/house-of-prime-rib-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 7287,
                    "categories": [
                        {
                            "alias": "tradamerican",
                            "title": "American (Traditional)"
                        },
                        {
                            "alias": "steak",
                            "title": "Steakhouses"
                        },
                        {
                            "alias": "wine_bars",
                            "title": "Wine Bars"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.79338,
                        "longitude": -122.4225
                    },
                    "transactions": [],
                    "price": "$$$",
                    "location": {
                        "address1": "1906 Van Ness Ave",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94109",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "1906 Van Ness Ave",
                            "San Francisco, CA 94109"
                        ]
                    },
                    "phone": "+14158854605",
                    "display_phone": "(415) 885-4605",
                    "distance": 2107.975988721711
                },
                {
                    "id": "gR9DTbKCvezQlqvD7_FzPw",
                    "alias": "north-india-restaurant-san-francisco",
                    "name": "North India Restaurant",
                    "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/8713LkYA3USvWj9z4Yokjw/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/north-india-restaurant-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1762,
                    "categories": [
                        {
                            "alias": "indpak",
                            "title": "Indian"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.787789124691,
                        "longitude": -122.399305736113
                    },
                    "transactions": [
                        "restaurant_reservation",
                        "pickup",
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "123 Second St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94105",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "123 Second St",
                            "San Francisco, CA 94105"
                        ]
                    },
                    "phone": "+14153481234",
                    "display_phone": "(415) 348-1234",
                    "distance": 116.62854375567585
                },
                {
                    "id": "yn5gA62ekL-TzKBBI_rq-A",
                    "alias": "boba-guys-san-francisco-6",
                    "name": "Boba Guys",
                    "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/tY1HyU-Rzz0usQ7vqQ8kVQ/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/boba-guys-san-francisco-6?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 1992,
                    "categories": [
                        {
                            "alias": "coffee",
                            "title": "Coffee & Tea"
                        },
                        {
                            "alias": "bubbletea",
                            "title": "Bubble Tea"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.79,
                        "longitude": -122.40729
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$",
                    "location": {
                        "address1": "429 Stockton St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94108",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "429 Stockton St",
                            "San Francisco, CA 94108"
                        ]
                    },
                    "phone": "+14159672622",
                    "display_phone": "(415) 967-2622",
                    "distance": 732.0167617794144
                },
                {
                    "id": "Tttm1r5Dw1sRMCfc3EQWPw",
                    "alias": "boulevard-san-francisco",
                    "name": "Boulevard",
                    "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/WIdbLOHw8l4I8EVXX441Sw/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/boulevard-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 2665,
                    "categories": [
                        {
                            "alias": "newamerican",
                            "title": "American (New)"
                        }
                    ],
                    "rating": 4.0,
                    "coordinates": {
                        "latitude": 37.79337,
                        "longitude": -122.39274
                    },
                    "transactions": [
                        "delivery"
                    ],
                    "price": "$$$",
                    "location": {
                        "address1": "1 Mission St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94105",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "1 Mission St",
                            "San Francisco, CA 94105"
                        ]
                    },
                    "phone": "+14155436084",
                    "display_phone": "(415) 543-6084",
                    "distance": 956.0267604505011
                },
                {
                    "id": "ka1_lat2boQwLMsOCiwGiA",
                    "alias": "r-and-g-lounge-san-francisco",
                    "name": "R&G Lounge",
                    "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/MvC2vshFjeaY8DATKk_ZVA/o.jpg",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/r-and-g-lounge-san-francisco?adjust_creative=Htv-BsJZKGwBnCgfn6pevA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=Htv-BsJZKGwBnCgfn6pevA",
                    "review_count": 4494,
                    "categories": [
                        {
                            "alias": "seafood",
                            "title": "Seafood"
                        },
                        {
                            "alias": "cantonese",
                            "title": "Cantonese"
                        }
                    ],
                    "rating": 3.5,
                    "coordinates": {
                        "latitude": 37.7941240989441,
                        "longitude": -122.404937406342
                    },
                    "transactions": [
                        "restaurant_reservation",
                        "delivery"
                    ],
                    "price": "$$",
                    "location": {
                        "address1": "631 Kearny St",
                        "address2": "",
                        "address3": "",
                        "city": "San Francisco",
                        "zip_code": "94108",
                        "country": "US",
                        "state": "CA",
                        "display_address": [
                            "631 Kearny St",
                            "San Francisco, CA 94108"
                        ]
                    },
                    "phone": "+14159827877",
                    "display_phone": "(415) 982-7877",
                    "distance": 915.892514520641
                }
            ],
            "total": 4000,
            "region": {
                "center": {
                    "longitude": -122.399972,
                    "latitude": 37.786882
                }
            }
        },
        reservations: [],
        favorites: [],
        searchKey: "",
        dataLoaded: false,
        categoriesList: [],
        showCategories: [],
        ratingArray: [1, 2, 3, 4, 5],
        ratingArrayFiltered: [1, 5]
    },
    getters: {
        allRestaurant: (state) => state.restaurant,
        // getCategories: (state) => {
        //     console.log('before dataloaded')
        //     if (state.dataLoaded) {
        //         console.log('after dataloaded')
        //         return state.restaurant.businesses.forEach(item => {
        //             item.categories.forEach(element => {
        //                 if (!state.categoriesList.includes(element.title)) {
        //                     state.categoriesList.push(element.title)
        //                 }
        //             });
        //         })
        //     }
        // },
        filteredRestaurant: (state) => {
            if (state.dataLoaded) {

                let filteredData = [];
                let filteredDataCategories = [];
                let rating = [];

                filteredData = state.restaurant.businesses.filter(item => {
                    return item.name.toLowerCase().includes(state.searchKey.toLowerCase())
                });

                if (state.showCategories.length > 0) {
                    filteredData.filter(element => {
                        element.categories.forEach(categorie => {
                            if (state.showCategories.includes(categorie.title)) {
                                if (!filteredDataCategories.includes(element)) {
                                    filteredDataCategories.push(element);
                                    filteredData = filteredDataCategories;
                                }
                            }
                        })
                    });
                }

                filteredData.filter(element => {
                    if (element.rating >= state.ratingArrayFiltered[0] && element.rating <= state.ratingArrayFiltered[1]) {
                        rating.push(element);
                    }
                    filteredData = rating;
                });

                return filteredData
            }
        },
        allDetails: (state) => state.details
    },
    mutations: {

        UPDATE_RATING_FILTER(state, selectRating) {
            state.ratingArrayFiltered = selectRating;
        },

        UPDATE_SHOW_CATEGORIES(state, selectCat) {
            state.showCategories = selectCat
        },
        UPDATE_SEARCH_KEY(state, searchKey) {
            state.searchKey = searchKey
        },
        LOAD_RESTAURANT(state, data) {

            // by pass les problemes de cors en integrant des données physiques
            //state.restaurant = {...data};
            state.dataLoaded = true;
            console.log(data);

            // patch car getter pas appelé étrangement
            if (state.dataLoaded) {
                return state.restaurant.businesses.forEach(item => {
                    item.categories.forEach(element => {
                        if (!state.categoriesList.includes(element.title)) {
                            state.categoriesList.push(element.title)
                        }
                    });
                })
            }
        },
        LOAD_DETAILS(state, data) {
            state.details = {...data};
        },
        LOAD_FAV(state, data) {
            state.favorites = [...state.favorites, {...data}];
        },
        ADD_TO_FAVORITES(state, data) {
            state.favorites = [...state.favorites, {...data}];
        },
        REMOVE_FROM_FAVORITES(state, data) {
            let removeIndex = state.favorites.map(item => {
                return item.id;
            }).indexOf(data.id);
            state.favorites.splice(removeIndex, 1);
        },
        ADD_RESERVATION(state, reservation) {
            state.reservations = [...state.reservations, {...reservation, id: state.reservations.length}]
        }
    },
    actions: {
        add_to_favorites({commit}, payload) {
            commit('ADD_TO_FAVORITES', payload)
        },
        remove_from_favorites({commit}, payload) {
            commit('REMOVE_FROM_FAVORITES', payload);
        },

        async load_restaurant({commit}) {
            //await apiService.fetchData("/businesses/search", "?latitude=37.786882&longitude=-122.399972").then(response => commit("LOAD_RESTAURANT", response))
            commit("LOAD_RESTAURANT")
        },
        load_details({commit}, payload) {
            apiService.fetchData("/businesses/", payload.id).then(response => commit("LOAD_DETAILS", response))
        },
        add_reservation({commit}, payload) {
            commit('ADD_RESERVATION', payload);
            const key = db.ref().child('reservations').push().key;
            db.ref('reservations').child(key).update({content: payload});
        }
    }
})
